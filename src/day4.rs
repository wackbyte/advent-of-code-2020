use std::collections::HashMap;

#[aoc_generator(day4)]
fn input_generator(input: &str) -> Vec<HashMap<String, String>> {
    input
        .split("\n\n")
        .map(|p| {
            let mut passport = HashMap::new();
            for (key, value) in p.split('\n').flat_map(|s| s.split(' ')).map(|f| {
                let mut split = f.split(":");
                (split.next().unwrap(), split.next().unwrap())
            }) {
                passport.insert(key.to_owned(), value.to_owned());
            }
            passport
        })
        .collect()
}

#[aoc(day4, part1)]
fn solve_part1(input: &[HashMap<String, String>]) -> usize {
    let mut counter = 0;
    for passport in input {
        let len = passport.len();
        let has_cid = passport.contains_key("cid");
        if len == 8 || (len == 7 && !has_cid) {
            counter += 1;
        }
    }
    counter
}

#[aoc(day4, part2)]
fn solve_part2(input: &[HashMap<String, String>]) -> usize {
    let mut counter = 0;
    for passport in input {
        if let (Some(byr), Some(iyr), Some(eyr), Some(hgt), Some(hcl), Some(ecl), Some(pid)) = (
            passport.get("byr"),
            passport.get("iyr"),
            passport.get("eyr"),
            passport.get("hgt"),
            passport.get("hcl"),
            passport.get("ecl"),
            passport.get("pid"),
        ) {
            if byr.len() == 4 {
                let num = byr.parse::<u16>().unwrap();
                if num >= 1920 && num <= 2002 {
                    if iyr.len() == 4 {
                        let num = iyr.parse::<u16>().unwrap();
                        if num >= 2010 && num <= 2020 {
                            if eyr.len() == 4 {
                                let num = eyr.parse::<u16>().unwrap();
                                if num >= 2020 && num <= 2030 {
                                    let (unit, value) = match hgt.len() {
                                        4 => (&hgt[2..], hgt[0..2].parse::<u8>().unwrap()),
                                        5 => (&hgt[3..], hgt[0..3].parse::<u8>().unwrap()),
                                        _ => continue,
                                    };

                                    match unit {
                                        "cm" => {
                                            if value < 150 || value > 193 {
                                                continue;
                                            }
                                        }
                                        "in" => {
                                            if value < 59 || value > 76 {
                                                continue;
                                            }
                                        }
                                        _ => continue,
                                    }

                                    if hcl.starts_with('#')
                                        && hcl.len() == 7
                                        && hcl[1..]
                                            .chars()
                                            .all(|c| matches!(c, '0'..='9' | 'a'..='f'))
                                    {
                                        if ecl == "amb"
                                            || ecl == "blu"
                                            || ecl == "brn"
                                            || ecl == "gry"
                                            || ecl == "grn"
                                            || ecl == "hzl"
                                            || ecl == "oth"
                                        {
                                            if pid.len() == 9 {
                                                counter += 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    counter
}
