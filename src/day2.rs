use std::{
    convert::{TryFrom, TryInto},
    error::Error,
    fmt::{Display, Formatter},
};

#[derive(Debug, Default, Copy, Clone, Eq, PartialEq)]
struct NotALetter(char);

impl Display for NotALetter {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "not a letter: {}", self.0)
    }
}

impl Error for NotALetter {}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Letter {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
}

impl Letter {
    const fn into_char(self) -> char {
        match self {
            Letter::A => 'a',
            Letter::B => 'b',
            Letter::C => 'c',
            Letter::D => 'd',
            Letter::E => 'e',
            Letter::F => 'f',
            Letter::G => 'g',
            Letter::H => 'h',
            Letter::I => 'i',
            Letter::J => 'j',
            Letter::K => 'k',
            Letter::L => 'l',
            Letter::M => 'm',
            Letter::N => 'n',
            Letter::O => 'o',
            Letter::P => 'p',
            Letter::Q => 'q',
            Letter::R => 'r',
            Letter::S => 's',
            Letter::T => 't',
            Letter::U => 'u',
            Letter::V => 'v',
            Letter::W => 'w',
            Letter::X => 'x',
            Letter::Y => 'y',
            Letter::Z => 'z',
        }
    }

    const fn from_char(c: char) -> Option<Self> {
        match c {
            'a' => Some(Letter::A),
            'b' => Some(Letter::B),
            'c' => Some(Letter::C),
            'd' => Some(Letter::D),
            'e' => Some(Letter::E),
            'f' => Some(Letter::F),
            'g' => Some(Letter::G),
            'h' => Some(Letter::H),
            'i' => Some(Letter::I),
            'j' => Some(Letter::J),
            'k' => Some(Letter::K),
            'l' => Some(Letter::L),
            'm' => Some(Letter::M),
            'n' => Some(Letter::N),
            'o' => Some(Letter::O),
            'p' => Some(Letter::P),
            'q' => Some(Letter::Q),
            'r' => Some(Letter::R),
            's' => Some(Letter::S),
            't' => Some(Letter::T),
            'u' => Some(Letter::U),
            'v' => Some(Letter::V),
            'w' => Some(Letter::W),
            'x' => Some(Letter::X),
            'y' => Some(Letter::Y),
            'z' => Some(Letter::Z),
            _ => None,
        }
    }
}

impl Display for Letter {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.into_char())
    }
}

impl From<Letter> for char {
    fn from(letter: Letter) -> Self {
        match letter {
            Letter::A => 'a',
            Letter::B => 'b',
            Letter::C => 'c',
            Letter::D => 'd',
            Letter::E => 'e',
            Letter::F => 'f',
            Letter::G => 'g',
            Letter::H => 'h',
            Letter::I => 'i',
            Letter::J => 'j',
            Letter::K => 'k',
            Letter::L => 'l',
            Letter::M => 'm',
            Letter::N => 'n',
            Letter::O => 'o',
            Letter::P => 'p',
            Letter::Q => 'q',
            Letter::R => 'r',
            Letter::S => 's',
            Letter::T => 't',
            Letter::U => 'u',
            Letter::V => 'v',
            Letter::W => 'w',
            Letter::X => 'x',
            Letter::Y => 'y',
            Letter::Z => 'z',
        }
    }
}

impl TryFrom<char> for Letter {
    type Error = NotALetter;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        Self::from_char(c).ok_or(NotALetter(c))
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct Policy {
    min: usize,
    max: usize,
    letter: Letter,
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Entry {
    policy: Policy,
    password: Vec<Letter>,
}

impl Display for Entry {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}-{} {}: {}",
            self.policy.min,
            self.policy.max,
            self.policy.letter,
            self.password
                .iter()
                .map(|letter| letter.into_char())
                .collect::<String>()
        )
    }
}

#[aoc_generator(day2)]
fn input_generator(input: &str) -> Vec<Entry> {
    input
        .lines()
        .map(|l| {
            let mut terms = l.split(' ');
            let mut range = terms.next().unwrap().split('-');
            let min = range.next().unwrap().parse().unwrap();
            let max = range.next().unwrap().parse().unwrap();
            let letter = terms
                .next()
                .unwrap()
                .chars()
                .next()
                .unwrap()
                .try_into()
                .unwrap();
            let password = terms
                .next()
                .unwrap()
                .chars()
                .map(|c| c.try_into().unwrap())
                .collect();
            Entry {
                policy: Policy { min, max, letter },
                password,
            }
        })
        .collect()
}

#[aoc(day2, part1)]
fn solve_part1(input: &[Entry]) -> usize {
    input
        .iter()
        .filter(|entry| {
            let mut counter = 0;
            for letter in &entry.password {
                if *letter == entry.policy.letter {
                    counter += 1
                }
            }
            counter >= entry.policy.min && counter <= entry.policy.max
        })
        .count()
}

#[aoc(day2, part2)]
fn solve_part2(input: &[Entry]) -> usize {
    input
        .iter()
        .filter(|entry| {
            match (
                entry.password.get(entry.policy.min - 1),
                entry.password.get(entry.policy.max - 1),
            ) {
                (Some(x), Some(y)) => (*x == entry.policy.letter) ^ (*y == entry.policy.letter),
                _ => false,
            }
        })
        .count()
}
