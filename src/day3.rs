use std::ops::{Add, AddAssign, Index, IndexMut, Sub, SubAssign};

#[derive(Debug, Default, Copy, Clone, Eq, PartialEq)]
struct Position {
    x: usize,
    y: usize,
}

impl Position {
    const fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }
}

impl Add for Position {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self::new(self.x + other.x, self.y + other.y)
    }
}

impl AddAssign for Position {
    fn add_assign(&mut self, other: Self) {
        *self = *self + other;
    }
}

impl Sub for Position {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self::new(self.x - other.x, self.y - other.y)
    }
}

impl SubAssign for Position {
    fn sub_assign(&mut self, other: Self) {
        *self = *self - other;
    }
}

#[derive(Debug, Clone)]
struct Map {
    /// 2D array ordered by rows.
    /// `true` is a tree, and `false` is an open square.
    pattern: Vec<bool>,
    width: usize,
    height: usize,
}

impl Map {
    fn position_to_index(&self, position: Position) -> usize {
        let x = position.x % self.width;
        let y = position.y % self.height;
        y * self.width + x
    }
}

impl Index<Position> for Map {
    type Output = bool;

    fn index(&self, position: Position) -> &Self::Output {
        &self.pattern[self.position_to_index(position)]
    }
}

impl IndexMut<Position> for Map {
    fn index_mut(&mut self, position: Position) -> &mut Self::Output {
        let index = self.position_to_index(position);
        &mut self.pattern[index]
    }
}

#[aoc_generator(day3)]
fn input_generator(input: &str) -> Map {
    let height = input.lines().count();
    let width = input.lines().next().unwrap().len();
    let pattern = input
        .lines()
        .flat_map(|l| l.chars())
        .map(|c| match c {
            '.' => false,
            '#' => true,
            _ => unreachable!("input pattern should only contain either `.` or `#`"),
        })
        .collect();
    Map {
        pattern,
        width,
        height,
    }
}

#[aoc(day3, part1)]
fn solve_part1(map: &Map) -> usize {
    const SLOPE: Position = Position::new(3, 1);

    let mut position = Position::default();
    let mut counter = 0;
    while position.y < map.height {
        position += SLOPE;
        if map[position] {
            counter += 1;
        }
    }
    counter
}

#[aoc(day3, part2)]
fn solve_part2(map: &Map) -> usize {
    const SLOPES: &[Position] = &[
        Position::new(1, 1),
        Position::new(3, 1),
        Position::new(5, 1),
        Position::new(7, 1),
        Position::new(1, 2),
    ];

    let mut all = 1;
    for slope in SLOPES {
        let mut position = Position::default();
        let mut counter = 0;
        while position.y < map.height {
            position += *slope;
            if map[position] {
                counter += 1;
            }
        }
        all *= counter;
    }
    all
}
