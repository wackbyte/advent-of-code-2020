#[aoc_generator(day1)]
fn input_generator(input: &str) -> Vec<u64> {
    input.lines().map(|l| l.parse().unwrap()).collect()
}

#[aoc(day1, part1)]
fn solve_part1(input: &[u64]) -> u64 {
    for (i, x) in input.iter().enumerate() {
        for (j, y) in input.iter().enumerate() {
            if i != j {
                if x + y == 2020 {
                    return x * y;
                }
            }
        }
    }
    unreachable!("there should be two entries that sum to 2020");
}

#[aoc(day1, part2)]
fn solve_part2(input: &[u64]) -> u64 {
    for (i, x) in input.iter().enumerate() {
        for (j, y) in input.iter().enumerate() {
            for (k, z) in input.iter().enumerate() {
                if i != j && j != k && k != i {
                    if x + y + z == 2020 {
                        return x * y * z;
                    }
                }
            }
        }
    }
    unreachable!("there should be two entries that sum to 2020");
}
